package practica.com.sintaxisBasica;

public class Cliente {

	public int codigo;
	public String nombre;
	public String apellido;
	public String email;
	public long telefono;
	public boolean nuevo;
	public double cifraVentas;
	public Cliente() {
		
	} 
	
	public Cliente(int codigo,String nombre, String email) {
		super();
		this.codigo=codigo;
		this.nombre= nombre;
		this.email=email;
	}

	public Cliente(int codigo, String nombre, String apellido, String email, long telefono, boolean nuevo,
			double cifraVentas) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.nuevo = nuevo;
		this.cifraVentas = cifraVentas;
	}
	
	
	public void mostrarDatos() {
		System.out.println("C�digo= "+codigo+" Nombre= "+nombre+" ....");
	}
	public void cambiarTelefono(long nuevoTelefono) {
		this.telefono=nuevoTelefono;
	}
	
}
