package practica.com.sintaxisBasica;

import javax.swing.JOptionPane;

public class PruebaSwitch {

	private int numeros;

	public void calcularMeses() {
		do {
			numeros = Integer.parseInt(JOptionPane.showInputDialog("Ingrese n�mero"));
		} while (numeros <= 0 || numeros > 12);

		switch (numeros) {
		case 4:
		case 5:
		case 6:
			System.out.println("Estos meses son de Primavera");
			break;
		case 7:
		case 8:
		case 9:
			System.out.println("Estos meses son de verano");
			break;
		case 10:
		case 11:
		case 12:
			System.out.println("Estos meses son de Oto�o");
			break;
		default:
			System.out.println("Estos meses son de Invierno");
			break;
		}

	}
}
