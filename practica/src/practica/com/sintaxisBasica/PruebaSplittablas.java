package practica.com.sintaxisBasica;


import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class PruebaSplittablas {

	private String hermanos="Nestor,Carlos,Elisa,Angelica,Elizabeth";
	
				
	
	public static void main(String[] args) {
		PruebaSplittablas prueba= new PruebaSplittablas();
		String brothers= prueba.getHermanos();
			
		String[] nestor=prueba.mostrarDatos(brothers);	
		for (String brot33hers : nestor) {
			System.out.println(brot33hers);
		}
		
		 Integer[] numeros= new Integer[3];
		 numeros[0]=25;
		 numeros[1]=6;
		 numeros[2]=36;
		 //numeros[3]=12;
		Integer ordenado[] =prueba.ordenarTabla(numeros);
		for (Integer numerosOrdenados : ordenado) {
			System.out.println(numerosOrdenados);
		}
		 
		
	
	}
	public String[] ordenarString(String datos[]) {
		Arrays.sort(datos);
		return datos;
		
	}
	public Integer[] ordenarTabla(Integer tabla[]) {
		Arrays.parallelSort(tabla);
		return tabla;
		
	}
	public String[] mostrarDatos(String nombres) {
		String arrayDatos[]=nombres.split(",");
		Arrays.parallelSort(arrayDatos);
		return arrayDatos;
		}
	
	public void setHermanos(String hermanos) {
		this.hermanos = hermanos;
	}
	public String getHermanos() {
		return hermanos;
	}
}
