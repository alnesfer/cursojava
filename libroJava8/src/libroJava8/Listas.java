package libroJava8;

import java.util.ArrayList;

public class Listas {


public  static void main(String args[]) {
	
	ArrayList<String> structure= new ArrayList<String>(200);
	structure.add("Nestor");
	structure.add("Eileen");
	structure.add("Katty");
	
	//System.out.println(structure.get(2));
		/*
		 * for (String contador : structure) { System.out.println(contador); }
		 */
	if(structure.contains("Katty")) {
		structure.remove("Katty");
	}
	for (String contador : structure) {
		System.out.println(contador);
	}
	System.out.println(structure.size());
}
}